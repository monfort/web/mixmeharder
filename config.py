import os
import secrets
DB_DIR = os.path.dirname(os.path.abspath(__file__))


class Config:
    SECRET_KEY = os.environ.get('SECRET_KEY') or secrets.token_hex(32)

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True
    DB_URI = 'sqlite:////{dbpath}'.format(
        dbpath=os.path.join(DB_DIR, 'dev.db.sqlite')
    )
    SQLALCHEMY_DATABASE_URI = DB_URI
    SQLALCHEMY_TRACK_MODIFICATIONS = False


# class TestingConfig(Config):
#     TESTING = True
#     SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or \
#         'sqlite:///' + os.path.join(basedir, 'data-test.sqlite')
#     WTF_CSRF_ENABLED = False


class ProductionConfig(Config):
    DEBUG = False
    DB_URI = 'sqlite:////{dbpath}'.format(
        dbpath=os.path.join(DB_DIR, 'prod.pyany.db.sqlite')
    )
    SQLALCHEMY_DATABASE_URI = DB_URI
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    @classmethod
    def init_app(cls, app):
        Config.init_app(app)
        pass


config = {
    'development': DevelopmentConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}