import os
from urllib.request import quote
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

from selenium.webdriver.chrome.options import Options

opts = Options()
opts.binary_location = "/Applications/Chromium.app/Contents/MacOs/Chromium"
# driver = webdriver.Chrome(chrome_options=opts)

# from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

# server_url = "http://%s:%s/wd/hub" % (test_host, test_port)
# dc = DesiredCapabilities.HTMLUNIT
# wd = webdriver.Remote(server_url, dc)
browser = \
    webdriver.Chrome(
        os.path.join(
            os.getcwd(),
            'chromedriver'
        ),
        chrome_options=opts)
# browser = webdriver.Remote(
  # desired_capabilities=webdriver.DesiredCapabilities.HTMLUNIT)

# textToSearch = '4k drone footage above -loop -"Nature Relaxation" ' + \
#                '-review -tutorial -"Above &", long'

# textToSearch = 'crash car -beamng -minecraft -SIMULATOR -prank -i, long'

# textToSearch = 'pursuit chase police, long'

# textToSearch = 'best dance performances -music, long'
#textToSearch = 'best hip hop popping locking -talk -club -mix, long'

textToSearch = ''
# textToSearch = 'juste debout -talk, long'

query = quote(textToSearch)
# url = "https://www.youtube.com/results?search_query=" + query
# url = "https://www.youtube.com/user/wwwy2000/videos" + query
#url = "https://www.youtube.com/playlist?list=PL7miQraiB6LTix4hdk-RttTNtBi1jbXC2" + query
# url = "https://www.youtube.com/playlist?list=PLt-iFehsXPBq7Crm3FydbpYztbq2yBWTP"
# url = "https://www.youtube.com/playlist?list=PLj8AFpGjDW8nlzj5ir8He1jWjU1P69wI9"
# url = "https://www.youtube.com/playlist?list=PL_T9MO520krq5QsT1sIHdmBUNodksi8v2"
# url = "https://www.youtube.com/playlist?list=PLiaS5yI_XV2VL99hJx_bJEiZ4Tp8JSKaH"
# url = "https://www.youtube.com/playlist?list=PLLrl0bmypnr-_ZHnYvJqJeu0CEdvyY31S"
# url = "https://www.youtube.com/playlist?list=PL5Eon4RWgrSxPbq5ArWyrkBoBnf8XlCKu"
# url = "https://www.youtube.com/playlist?list=PLjjPPHSTiy1SUzXa8X1_gmVuTKvWsO4H1"
# url = "https://www.youtube.com/playlist?list=PLl1XyDt5dNg4tKnogabMegr8qSXfePIUL"
# url = "https://www.youtube.com/playlist?list=PLBxTahny9StEkJVo612-tv-zvMGy4EE2x"


PLAYLISTNAME = "wild-life2018"
pl_filename = "{p}.txt".format(p=PLAYLISTNAME)
playlists =[
    "https://www.youtube.com/playlist?list=PLjjPPHSTiy1S29DnhCfD0oflDpvX_gWIX",
]
shortlen=len("https://www.youtube.com/watch?v=")


for url in playlists:
    browser.get(url)
    time.sleep(1)
    elem = browser.find_element_by_tag_name("body")

    no_of_pagedowns = 30
    while no_of_pagedowns:
        elem.send_keys(Keys.PAGE_DOWN)
        time.sleep(0.5)
        no_of_pagedowns-=1
    time.sleep(4)
    html = browser.page_source

    # titles = browser.find_elements_by_css_selector("#video-title")
    titles = browser.find_elements_by_css_selector("ytd-playlist-video-renderer.style-scope a#thumbnail")
    # print(str(len(titles)) + " videos")

    # div.ytd-playlist-sidebar-renderer > a:nth-child(1)
    # ytd-playlist-video-renderer.style-scope:nth-child(1) > div:nth-child(2) > a:nth-child(1) > ytd-thumbnail:nth-child(1) > a:nth-child(1)

    with open(pl_filename, "a") as playliststream:
        # for playlists
        for t in titles:
            t_url = t.get_attribute("href")
            if t_url is not None:
                t_id = t_url.split("&")[0][shortlen:]
                print(t_id)
                playliststream.write("{tid}\n".format(tid=t_id))

# for searches
# for elem in titles:
    # print(elem.get_attribute("href")[shortlen:])

browser.close()
browser.quit()

