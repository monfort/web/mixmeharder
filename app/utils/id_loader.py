import os
# print(os.path.dirname(os.path.realpath(__file__)))

def load_id_files(fname='all-yt-ids.txt'):
    rootp = os.path.dirname(os.path.realpath(__file__))
    idfpath = os.path.join(rootp, fname)
    id_lines = []
    yt_ids = []
    # print('all-ty-ids.txt')
    try:
        with open(idfpath, 'r') as ids_file:
            id_lines = ids_file.readlines()
    except FileNotFoundError as fnferr:
        print("Create the youtube video ids file with our webscrapper...")
    else:
        yt_ids = [ytid.strip() for ytid in id_lines]
    return list(set(yt_ids))
