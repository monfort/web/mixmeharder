
var COOKIE_AUDIO = 'mmh-autoplayaudio';
var COOKIE_VIDEO = 'mmh-autoplayvideo';
var COOKIE_START_TIME = 'strtime'
var playImgSrc;
var pauseImgSrc;

var isLiveFeed = false;
var vidStartTime;

var tag = document.createElement('script');
tag.src = "https://www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;
var iframe;

function onPlayerStateChange(evt){
    if (evt.data == YT.PlayerState.ENDED) {
        loadNext();
    }
    // if (evt.data == YT.PlayerState.PLAYING) {
    //     if (cookieIsSet("gm-autoplayvideo")){
    //         evt.target.playVideo();
    //     } else {
    //         evt.target.stopVideo();
    //     }
    // }
}

function initYoutubePlayer(videoId, autoPlay, startTime, firstInPlayist){
    autoPlay = autoPlay === true ? 1 : 0;
    return new YT.Player('video-placeholder', {
        videoId: videoId,
        playerVars: {
            autohide: 2,
            autoplay: autoPlay,
            // autoplay: autoPlay,
            cc_load_policy: 0,
            color: 'white',
            controls: 1,
            disablekb: 0,
            start: startTime,
            enablejsapi: 1,
            // end: number,
            fs: 1,
            hl: 'en',
            iv_load_policy: 3,
            modestbranding: 1,
            origin: 'http://127.0.0.1:5000',
            playsinline: 0,
            rel: 0,
            showinfo: 0,
            theme: 'dark'
        },
        events: {
            'onReady': function(evt){
                if (firstInPlayist === true){
                    $('#prev-vid').addClass("first-vid");
                } else {
                    $('#prev-vid').removeClass("first-vid");
                }
                // evt.target.playVideo();
                evt.target.mute();
            },
            'onStateChange': onPlayerStateChange
        }
    });
}


function onYouTubePlayerAPIReady() {
    if ($('#video-placeholder').length){
        $.get(
            Flask.url_for("main.go_to_category", {"init" : true}),
            function(data) {
                var category = data['category'];
                var videoId = data['video_id'];
                var firstInPlayist = data['first'];
                var autoPlay = cookieIsSet(COOKIE_VIDEO);
                isLiveFeed = data['livefeed'];
                var vidStartTime =
                    isLiveFeed === true ?
                        0 : parseInt(getCookie(COOKIE_START_TIME)) || 0;
                // vidStartTime = parseInt(getCookie(COOKIE_START_TIME)) || 0;
                // var autoplay = cookieIsSet(COOKIE_VIDEO) === true ? 1 : 0 ;
                // $.when(
                player = initYoutubePlayer(videoId,
                                           autoPlay,
                                           vidStartTime,
                                           firstInPlayist);
                // ).then(
                //     function(){
                //         loadVideoById(videoId);
                //     }
                // );
                // player.setPlaybackQuality('144p');
            }
        );
    }
}


function onLoadCategory(){

}

function loadNext(){
    return loadVideo(route="main.get_next");
}

function loadPrev(){
    return loadVideo(route="main.get_prev");
}

function loadVideoById(video_id){
    $.when(
        player.loadVideoById(video_id)
    ).then(
        function(){
            unsetCookie(COOKIE_START_TIME);
        }
    );
}

function loadVideo(route="main.get_next"){
    var current_video = player.getVideoData()['video_id']
    $.get(
        Flask.url_for(route, {"curr_ytid" : current_video}),
        function(data) {
            // vidId = data['videoid'];
            loadVideoById(data['videoid']);
            showControls();
            if (data['is_first'] === true){
                $('#prev-vid').addClass("first-vid");
            } else {
                $('#prev-vid').removeClass("first-vid");
            }
        }
    );
}


function getCookie(cookieName){
    return Cookies.get(cookieName);
}

function cookieIsSet(cookieName){
    return getCookie(cookieName) === '1';
}

function setCookie(cookieName, cookieValue="1"){
    return Cookies.set(cookieName, cookieValue,
                       { expires: 7, path:'/' });
}
function unsetCookie(cookieName){
    return Cookies.remove(cookieName, { path: '/' });
}

function initCheckbox(checkboxObj, cookieName){
    if (checkboxObj.length){ // only at /mood
        if (cookieIsSet(cookieName)){
            checkboxObj.prop('checked', true);
        }
        else {
            checkboxObj.prop('checked', false);
        }
    }
}

function initCookies(checkboxObj, cookieName){
    if(checkboxObj.is(':checked')){
        setCookie(cookieName);
    }
}


var orginalControlDisp;
function initSession(){
    // check / set cookies
    orginalControlDisp = $('.floatingcontrol').css("display");
    autoVid = $('#mmh input[name=check-video]');
    autoAud = $('#mmh input[name=check-audio]');
    initCookies(autoVid, COOKIE_VIDEO);
    initCookies(autoAud, COOKIE_AUDIO);
    initCheckbox(autoVid, COOKIE_VIDEO);
    initCheckbox(autoAud, COOKIE_AUDIO);
}

function startAudio(){
    $('#audio-player').trigger("play");
    $('#toggle-music img').attr("src", pauseImgSrc);
    setCookie(COOKIE_AUDIO);
}
function stopAudio(){
    $('#audio-player').trigger("pause");
    $('#toggle-music img').attr("src", playImgSrc);
    unsetCookie(COOKIE_AUDIO);
}

function toggleAudio(){
    if ( $('#audio-player').prop("paused") ){
        startAudio();
    } else {
        stopAudio();
    }
}

$(function() {
    var buttonImg = $('#toggle-music img');
    playImgSrc = Flask.url_for("static", {'filename' : "img/controls/ic_play_arrow_black_24px.svg"});
    pauseImgSrc = Flask.url_for("static", {'filename' : "img/controls/ic_pause_black_24px.svg"});

    if ($(location).attr('pathname') === Flask.url_for("main.landing")){
        initSession();
        unsetCookie(COOKIE_START_TIME);
    }

    /*********************
    ** JS EVENT BINDING **
    *********************/

    $('#toggle-music').click(function() {
        toggleAudio()
    });

    $('#mmh input[name=check-audio]').change(function() {
        if (this.checked) {
            setCookie(COOKIE_AUDIO);
        } else {
            unsetCookie(COOKIE_AUDIO);
        }
    });

    $('#mmh input[name=check-video]').change(function() {
        if (this.checked) {
            setCookie(COOKIE_VIDEO);
        } else {
            unsetCookie(COOKIE_VIDEO);
        }
    });


    // $('#radiomeuh-lk').click(function() {
    //     // var user = $('#txtUsername').val();
    //     // var pass = $('#txtPassword').val();
    //     $.get(Flask.url_for("main.info", {station: "meuh"}), function(data) {
    //     });
    //     // $.ajax({
    //     //     url: '{{ url_for("info", station="meuh") }}',
    //     //     // url: '/info/meuh',
    //     //     // data: $('form').serialize(),
    //     //     type: 'POST',
    //     //     success: function(response) {
    //     //     },
    //     //     error: function(error) {
    //     //     }
    //     // });
    // });

    $('#next-vid').click(function() {
        loadNext();
    });

    $('#prev-vid').click(function() {
        loadPrev();
    });

    $('button[name="switch-station"]').click(function() {
        $('button[name=switch-station]').removeClass("active");
        $(this).addClass("active");
        var station_id = $(this).attr('id');
        current_flux = $('#audio-player').attr('src');
        $.get(
            Flask.url_for("main.get_flux", {"station": station_id}),
            function(data) {
                if (current_flux !== data['flux']){
                    $('#current-logo').css('background-image', 'url("' + data['logo'] + '")');
                    $('.disclaimer a').attr('href', data['url']);
                    $('.disclaimer span').text(data['disclaimer']);
                    $('#audio-player').attr('src', data['flux']);
                    startAudio();
                }
            }
        );
    });

    $('li[name=category]').click(function() {
        // $('li[name=category]').removeClass("selected");
        // $(this).addClass("selected");
        var current_video = player.getVideoData()['video_id'];
        var cat_id = $(this).attr('id');
        console.log("hey");
        $.get(
            Flask.url_for("main.go_to_category", {"category": cat_id}),
            function(data) {
                if (current_video !== data['video_id']){
                    loadVideoById(data['video_id']);
                    showControls();
                }
            }
        );
    });

    $(window).on('beforeunload', function(){
        if ($('#video-placeholder').length && player){
            var current_time_sec = parseInt(player.getCurrentTime())
            setCookie(COOKIE_START_TIME, current_time_sec);
            // setCookie("reload-strtime", current_time_sec);
            // setCookie("reload-", current_time_sec);
            // setCookie("reload-", current_time_sec);
        }
     });
    $(window).on('load', function(){
        // your logic here`enter code here`
        vidStartTime = getCookie(COOKIE_START_TIME);
        cookieAudioPlay = getCookie(COOKIE_AUDIO);
        // if cookieAudioPlay
        var autoPlayAudio = parseInt(getCookie(COOKIE_AUDIO)) || 0;
        if ($(location).attr('pathname') === Flask.url_for("main.index")){
            if (autoPlayAudio === 1){
                startAudio();
            } else {
                stopAudio();
            }
        }
    });


    $('[name=nav-menu-clickable]').click(function() {
        var clickable_id = $(this).attr("id");
        $('#' + clickable_id + '-modal').modal('show');
    });

    function reposition() {
        var modal = $(this),
            dialog = modal.find('.modal-dialog');
        modal.css('display', 'block');
        // modal.css('display', 'block');

        // Dividing by two centers the modal exactly, but dividing by three
        // or four works better for larger screens.
        dialog.css("margin-top", Math.max(0, (($(window).height() - dialog.height()) / 2)) + 10);
        // dialog.css("margin-top", "33%");
    }
    // Reposition when a modal is shown
    $('.modal').on('show.bs.modal', reposition);
    // Reposition when the window is resized
    $(window).on('resize', function() {
        $('.modal:visible').each(reposition);
    });
    // .lsidebar
});

function hideControls(){
    $('.floatingcontrol').css("display", "none");
}

function showControls(){
    $('.floatingcontrol').css("display", orginalControlDisp);
}



