from flask import Flask
# from flask_session import Session

# from flask_kvsession import KVSessionExtension
from flask_sqlalchemy import SQLAlchemy
from simplekv.db.sql import SQLAlchemyStore
from flask_kvsession import KVSessionExtension


from flask_bootstrap import Bootstrap
from flask_jsglue import JSGlue

from config import config


# db = SQLAlchemy(app)
# DB_DIR = os.path.dirname(os.path.abspath(__file__))
# DB_URI = 'sqlite:////{0}/test.db'.format(DB_DIR)
# app.config['SQLALCHEMY_DATABASE_URI'] = DB_URI

# @app.before_first_request
# def create_user():
#     db.create_all()


# appsession = Session()

bootstrap = Bootstrap()

# app = Flask(__name__)
# Check Configuration section for more details
# app.config.from_object(__name__)


def create_app(config_name):
    app = Flask(__name__)
    JSGlue(app)
    # app.config['SQLALCHEMY_DATABASE_URI'] = DB_URI

    db = SQLAlchemy()
    # Session(app)
    # print
    # sess = Session()
    # SESSION_TYPE = 'redis'
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    bootstrap.init_app(app)
    # appsession.init_app(app)
    # db.init_app(app)
    db.init_app(app)
    with app.app_context():
        store = SQLAlchemyStore(db.engine, db.metadata, 'sessions')
        db.create_all()
    # store.init_app(app)
    kvsession = KVSessionExtension(store)
    kvsession.init_app(app)

    from .main import main as main_blueprint
    app.register_blueprint(main_blueprint)

    return app
