from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, BooleanField, SelectField,\
    SubmitField
from wtforms.validators import Required, Length, Email, Regexp
from wtforms import ValidationError


class MoodForm(FlaskForm):
    mood = StringField('What is your mood?', validators=[Required()])
    submit = SubmitField('Submit')
