from flask import render_template
from flask import url_for
from flask import redirect
from flask import jsonify
from flask import session
from flask import request
from . import main
from .forms import MoodForm
from ..utils.id_loader import load_id_files

from random import randint, shuffle
import operator

MOODS = {}
CAT_CONFIG = {}
LIVEFEED_IDS = set()
YOUTUBE_IDS = []

# @main.before_first_request
# def create_user():


@main.before_app_first_request
def init_categories():

    global MOODS
    MOODS = {
        'meuh': {
            'url': "http://www.radiomeuh.com",
            'flux': "http://radiomeuh.ice.infomaniak.ch/radiomeuh-128.mp3",
            'logo': url_for('static', filename='img/logo/radiomeuh.svg'),
            'disclaimer': "Radio Meuh - La Clusaz",
            'modal_title': "Radio Meuuuh, an awesome French radio",
            'modal_content':
                "<p>Independent Internet radio station out of the French Alps playing fresh songs from all four corners of the globe. See their <a href='https://twitter.com/radiomeuh' target='_blank'>Twitter</a></p>",
            'extra_links': [
                [
                    "Donations",
                    "http://www.radiomeuh.com/faites-un-don/"
                ],
                [
                    "Shop",
                    "http://shop.radiomeuh.com/"
                ],
                [
                    "Contact",
                    "http://www.radiomeuh.com/contact/"
                ]
            ]

        },
        'active': {
            'url': "http://www.radioactivefm.gr",
            'flux': "http://46.165.220.22:8050/radioactive",
            'logo': url_for('static', filename='img/logo/radioactive913.png'),
            'disclaimer': "2015 RadioActive 91,3 - Sifnos.",
            'donations_link': "http://www.radioactivefm.gr",
            'modal_title': "Sifnos' Radioactive, an aweome greek radio",
            'modal_content':
                "<p>We knew Greece was awesome, this is yet another proof</p><p>From their wesite: « Sifnos island, July 1999, first time <em>On The Air</em>... Ever since Active, 24 hours, 365 days!<br/>Our music can be heard all over the rest of the Cycladic Islands (Mykonos, Syros, Serifos, Tinos, Paros, Naxos, Ios, Amorgos, Sikinos, Folegandros), to name a few. The music is eclectic and very particular. We believe that music is the international language, brings people from all over the world together and has no boundaries! The changes between music styles are very interesting (to say the least). RadioActive won't bore you! RadioActive's studio is located in Artemon (the most beautiful village of Sifnos) and it utilizes the latest in digital technology with unsurpassed audio quality! The signal is transferred via radio links from the studio to the highest mountain top in the island and from there is transmitted to the whole Aegean Sea.<br/>Link, Listen Live and interact socially with us because ... <b>Music Matters</b>»</p>",
            'extra_links': [
                [
                    "Donations (top right on homepage)",
                    "http://www.radioactivefm.gr"
                ],
                [
                    "Shop",
                    "http://www.radioactivefm.gr/index.php/k2-store-item"
                ],
                [
                    "Contact",
                    "http://www.radioactivefm.gr/index.php/home/contact-us"
                ]
            ]
        },
        'pbb': {
            'url': "http://www.laurentgarnier.com/PBB.html",
            'flux': "http://pbb.laurentgarnier.com:8000/pbb128",
            'logo': url_for('static', filename='img/logo/pbb.png'),
            'disclaimer': "Laurent Gernier's Pedro Basement Broadcast"
        }
    }

    global CAT_CONFIG
    CAT_CONFIG = {
        "livefeeds": {
            # discontinued
            # "tom": {
            #     "title": "Major Tom",
            #     "description": "Take your protein pills and put your helmet on...",
            #     "logo": url_for('static', filename='img/logo/car.png'),
            #     "video_id": "m2p55BmwmJM"
            # },
            "train": {
                "title": "train POV",
                "description": "Travel the world. Tchoo tchoooooooooo!",
                "logo": url_for('static', filename='img/logo/train.png'),
                "video_id": "DBd560d1DIM" # "uTpDWzfRMg8", #"kwoHU6pRjRA", "qghQ5eKGcyE"
            },
            # discontinued
            # "nasa247": {
            #     "title": "NASA livefeed 24/7",
            #     "description": "someone in space, filming himself 24/7. what a time to be alive",
            #     "logo": url_for('static', filename='img/logo/nasa.svg'),
            #     "video_id": "RtU_mdL2vBM"
            # },
            "pandas": {
                "title": "pandas 24/7",
                "description": "uh... pandas... 24/7... what a time to be alive ... ",
                "logo": url_for('static', filename='img/logo/panda2.svg'),
                "video_id": "p6Fl4bsgig0"
            },
            "shibuya": {
                "title": "tokyo's shibuya crossing 24/7",
                "description": "Live cam at Shibuya's crossing. Tokyo, Japan. IMPRESSIVE !",
                "logo": url_for('static', filename='img/logo/crossroad.svg'),
                "logo_credit" : "Smashicons from flaticons.com",
                "video_id": "PmrWwYTlAVQ"
            }
        },
        "preprocessed_categories": {
            "drone_footages": {
                "logo": url_for('static', filename='img/logo/drone2.svg'),
                "title": "drone footages",
                "description": "(mostly) drone videos",
                "video_ids": load_id_files("drone-better-list.txt")
            },
            "booty_shake": {
                "logo": url_for('static', filename='img/logo/ballerina.png'),
                "title": "booty shake",
                "description": "booty shake (no pr0n)",
                "video_ids": load_id_files("best-dances-yt-ids.txt")
            },
            "animals": {
                "logo": url_for('static', filename='img/logo/simba-raf-opt.png'),
                "title": "wild life",
                "description": "wild life videos of our animal counterparts",
                "video_ids": load_id_files("animals-yt-ids2018.txt")
            },
            "hot_pursuit": {
                "logo": url_for('static', filename='img/logo/sherif.png'),
                "title": "hot pursuit",
                "description": "crazy american hot pursuits",
                "video_ids": load_id_files("hot-pursuit-yt-ids.txt")
            }
        }
    }
    global LIVEFEED_IDS
    LIVEFEED_IDS = set(
        feed.get("video_id")
        for kfeed, feed in CAT_CONFIG.get("livefeeds").items()
    )
    global YOUTUBE_IDS
    YOUTUBE_IDS = \
        CAT_CONFIG.get("preprocessed_categories")\
        .get("drone_footages")\
        .get("video_ids")



@main.route('/getnasafeed', methods=['GET'])
def get_nasa():
    nasa_lf = CAT_CONFIG["livefeed"]["nasa247"]["video_id"] or None
    return jsonify({
        'videoid': nasa_lf
    })


@main.route('/')
def landing():
    return render_template('landing.html')


def get_channels_elements():
    defplay_logo = url_for(
        'static',
        filename='img/controls/ic_play_circle_filled_black_24px.svg'
    )
    channels = []
    chan = MOODS.items()
    for ck, c in chan:
        channels.append(
            {
                "id": ck,
                "text": "Nothing to say for now",
                "logo": c.get('logo', defplay_logo),
            }
        )
    return channels


# DEFAULT_CAT = "drone_footages"
DEFAULT_CAT = "train"

@main.route('/mood')
def redirect_mood():
    return redirect(url_for('main.landing'))

@main.route('/mix')
@main.route('/mix/<mood>')
def index(mood=None):
    rsidebar_chans = get_channels_elements()
    all_chans = [c["id"] for c in rsidebar_chans if "id" in c]
    if mood is not None and mood not in all_chans:
        return redirect(url_for('main.index'))
    radio = \
        all_chans[0] if len(all_chans) > 0 \
        else []

    lsidebar_elems = get_sidebar_elements()
    current_station = mood or "meuh"
    current_station_obj = MOODS.get(current_station)
    return render_template(
        'index.html',
        station_id=current_station,
        station=current_station_obj,
        all_stations=MOODS,
        rsidebar_channels=rsidebar_chans,
        lsidebar_elements=lsidebar_elems
    )


@main.route('/category', methods=['GET'])
@main.route('/category/<category>', methods=['GET'])
def go_to_category(category=None):
    livefeeds = CAT_CONFIG.get("livefeeds").keys()
    current_cat = session.get("current_category", None)
    if category is None and current_cat is not None:
        # reload
        category = current_cat
        id_to_play = session.get("current_id", None)
        is_livefeed = (current_cat in livefeeds)

    else:
        # init
        category = category or DEFAULT_CAT
        id_to_play, is_livefeed = init_category(category)

        if not id_to_play:
            # return a fail for a javascrip callback
            # return xyz, 203  # invalid request flag
            return \
                jsonify({
                    'status': 'NOTOK',
                    'reason': 'WTF: ID UNKNOWN man!'
                }), \
                203  # invalid request flag

    playlist_ids = session.get("playlist_ids", [])
    return jsonify({
        'category': category,
        'video_id': id_to_play,
        'livefeed': is_livefeed,
        'first':
            len(playlist_ids) == 1 or
            playlist_ids.index(id_to_play) == 0
    })


def init_category(cat):
    livefeeds = CAT_CONFIG.get("livefeeds").keys()
    is_livefeed = (cat in livefeeds)
    pcategories = CAT_CONFIG.get("preprocessed_categories").keys()
    id_to_play = None
    if cat in pcategories:
        # session["current_category"] = cat
        # here we cannot have a single ID
        # we have to init the session ids list
        # and get the first id from that list
        # dont forget to set this id as the current in session too
        id_to_play, playlist_ids = init_preproc_category_playlist(cat)
        # session["playlist_ids"] = playlist_ids

    elif is_livefeed:
        session["current_category"] = cat
        # here we can easily retrieve a single ID
        id_to_play = CAT_CONFIG.get("livefeeds").get(cat).get("video_id")
        playlist_ids = [id_to_play]  # livefeed is a unique ID in playlist
        # session["playlist_ids"] = playlist_ids
    # else:
        # playlist_ids = session["playlist_ids"]

    session["playlist_ids"] = playlist_ids  # a single one

    session["current_id"] = id_to_play  # set after from ajax?...
    return id_to_play, is_livefeed



def init_preproc_category_playlist(category):
    # init_preproc_category_playlist(cat)
    pp_category = CAT_CONFIG.get("preprocessed_categories", {}).get(category)

    if pp_category is not None:

        session["current_category"] = category
        cat_pl_ids = list(pp_category.get("video_ids", []))
        shuffle(cat_pl_ids)
        id_to_play = cat_pl_ids[0]

        # sess_ytids = list(YOUTUBE_IDS)
        # shuffle(sess_ytids)
        # session["playlist_ids"] = sess_ytids
        # curr_id = sess_ytids[0]
        # session["current_id"] = curr_id
        # curr_id = session.get("curr_id", None)
        # sess_ytids = session.get("playlist_ids", None)
    return id_to_play, cat_pl_ids



def get_sidebar_elements():
    defplay_logo = url_for(
        'static',
        filename='img/controls/ic_play_circle_filled_black_24px.svg'
    )
    livefeeds_els = []
    livefeeds = CAT_CONFIG.get("livefeeds").items()
    for lfk, lf in livefeeds:
        livefeeds_els.append(
            {
                "id": lfk,
                "title": lf.get('title', lfk),
                "description": lf.get('description', lfk),
                "logo": lf.get('logo', defplay_logo),
                "logo_credit": lf.get('logo_credit', None),
            }
        )
    categories_els = []
    categories = CAT_CONFIG.get("preprocessed_categories").items()
    for catk, cat in categories:
        categories_els.append(
            {
                "id": catk,
                "title": cat.get('title', catk),
                "description": cat.get('description', catk),
                "logo": cat.get('logo', defplay_logo),
                "logo_credit": cat.get('logo_credit', None),
            }
        )
    return livefeeds_els + categories_els



@main.route('/radio', methods=['GET'])
@main.route('/radio/<station>', methods=['GET'])
def radio(station=None):
    rad = MOODS.get(station, "active")
    return jsonify(rad)



@main.route('/station', methods=['GET'])
@main.route('/station/<station>', methods=['GET'])
def get_flux(station=None):
    if station is not None and station not in MOODS.keys():
        return redirect(url_for('main.index'))
    return jsonify(MOODS.get(station))


# def reset_list():


@main.route('/curr-video', methods=['GET'])
@main.route('/curr-video/<video_id>', methods=['GET'])
def current_id(video_id=None):
    if video_id is None:  # its a get
        all_ids = session.get("playlist_ids", [])
        video_id = session.get("curr_id", None)
        try:
            all_ids.index(video_id)
        except ValueError as vlrr:
            session["playlist_ids"] = None
            video_id, reset_ids = init_youtube_playlist()
        return jsonify({
            'videoid': video_id,
            'first': (all_ids.index(video_id) == 0),
        })
    elif video_id in LIVEFEEDS:
        session["playlist_ids"] = None
        video_id, reset_ids = init_youtube_playlist()
        session["current_id"] = video_id
        # session["playlist_ids"] = reset_ids
        return jsonify({
            'video_id': video_id,
            'first': (reset_ids.index(video_id) == 0),
        })
    # elif video_id == session["current_id"]:

    session["current_id"] = video_id
    return 'OK'


# @main.route('/next-video/<curr_ytid>', methods=['GET'])
# def get_next(curr_ytid):
#     vids = session.get("playlist_ids", YOUTUBE_IDS)
#     curr_index = vids.index(curr_ytid)
#     try:
#         next_id = vids[curr_index + 1]
#     except IndexError:
#         if curr_index + 1 == len(vids):
#             next_id = vids[0]

#     return jsonify({
#         'videoid': next_id
#     })


# def init_youtube_playlist():
#     curr_id = session.get("curr_id", None)
#     sess_ytids = session.get("playlist_ids", None)

#     if sess_ytids is None:
#         sess_ytids = list(YOUTUBE_IDS)
#         shuffle(sess_ytids)
#         session["playlist_ids"] = sess_ytids
#         curr_id = sess_ytids[0]
#         session["current_id"] = curr_id

#     return curr_id, sess_ytids


@main.route('/next-vid/<curr_ytid>', methods=['GET'])
def get_next(curr_ytid):
    sess_ytids = session.get("playlist_ids", None)
    # fist init
    if (curr_ytid == "null"):  # FIXME find a better test than null string
        curr_ytid = session.get("curr_id")
        idx_curr = sess_ytids.index(curr_ytid)
        vid, first = curr_ytid, idx_curr == 0
    else:
        vid, first = get_video_id(curr_ytid, operator.add)
    return jsonify(
        {
            'videoid': vid,
            'is_first': first
        }
    )


@main.route('/prev-vid/<curr_ytid>', methods=['GET'])
def get_prev(curr_ytid):
    vid, first = get_video_id(curr_ytid, operator.sub)
    return jsonify(
        {
            'videoid': vid,
            'is_first': first
        }
    )


# op=operator.add
# op=operator.sub
def get_video_id(curr_id, op=operator.add):
    # op(a,b)
    vids = session.get("playlist_ids")
    curr_index = vids.index(curr_id)
    try:
        video_id = vids[op(curr_index, 1)]
    except IndexError as idxerr:
        if op(curr_index, 1) == len(vids):
            video_id = vids[0]
    return video_id, (vids.index(video_id) == 0)

# @main.route('/prev-video/<curr_ytid>', methods=['GET'])
# def previous_video(curr_ytid):
#     vids = session.get("playlist_ids", YOUTUBE_IDS)
#     curr_ytid = session.get("current_ytid")
#     prev_id, first = get_previous(curr_ytid, vids)
#     return jsonify({
#         'videoid': prev_id,
#         'fisrt': first
#     })

# @main.route('/prev-video/<curr_ytid>', methods=['GET'])
# def get_prev(curr_ytid, all_ytids):
#     curr_index = all_ytids.index(curr_ytid)
#     try:
#         prev_id = vids[curr_index - 1]
#     except IndexError:
#         prev_id = vids[0]
#     return prev_id, curr_index == 0

# @main.route('/get_nextprev/<curr_id>', methods=['GET'])
# def get_nextprev_video_ids(current_id=curr_id):
#     sess_ytids = session.get("playlist_ids", None)
#     idx_curr = sess_ytids.index(current_id)
#     prev_id = \
#         sess_ytids[idx_curr - 1] or \
#         YOUTUBE_IDS[randint(0, LEN_IDS - 1)]
#     next_id = \
#         sess_ytids[idx_curr + 1] or \
#         YOUTUBE_IDS[randint(0, LEN_IDS - 1)]
#     return jsonify({
#         'prev_id': prev_id,
#         'next_id': next_id
#     })

